from sys import path
from radialColorHistogram.color_density import ColorDensity
from radialColorHistogram.radial_split import RadialSplitter
from aiohttp import web
from aiohttp.web_request import Request
import joblib
import requests
import tempfile
import aiohttp

async def handle(request: Request):
    if request.body_exists:
        try:
            dict = await request.json()
            url = dict.get('url', '')
            
            if len(url) < 0:
                return web.Response(text="ehlo")
            
            image = requests.get(url)
            tmpFile = tempfile.NamedTemporaryFile(mode='wb')
            tmpFile.write(image.content)
            paths = [tmpFile.name]

            testImage = request.app['colorDensity'].transform(paths)
            prediction = request.app['model'].predict(testImage)

            print(f"{url} : {prediction[0]}")

            return web.Response(text=prediction[0])

        except Exception as e:
            print(f"Wrong data format! Exception: {e}")

    return web.Response(text="ehlo")

def main():
    print("[MEME-API] Image analyzer")

    loadedModel = []
    loadedScaler = []

    try:
        loadedModel = joblib.load("model_ccc.dat")
    except:
        print("Can't load model!")
        return
    
    try:
        loadedScaler = joblib.load("scaler_ccc.dat")
    except:
        print("Can't load scaler!")
        return
    
    rs = RadialSplitter(nrings=1, nqslices=1)
    cd = ColorDensity(splitter=rs, color_model='HSV', n_bins=8, scaler=loadedScaler, log_transform=True)

    app = web.Application()
    app['colorDensity'] = cd
    app['model'] = loadedModel
    app['classes'] = loadedModel.classes_

    app.router.add_post('/', handle)
    try:
        web.run_app(app, port=4041)
    except Exception as e:
        print(f"Exception: {e}")
    print("\nServer stopped by user")

if __name__ == "__main__":
    main()
