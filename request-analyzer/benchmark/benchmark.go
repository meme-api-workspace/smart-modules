package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

const (
	RequestCount = 500

	requestTemplate1 = "http://localhost:4040/?name=Test_%d"

	requestTemplate2 = "http://localhost:4040/?request=нормализуй_мне_запрос"
)

func main() {
	fmt.Println("[MEME-API] Aiohttp tests 1")

	resp, err := http.Get(requestTemplate2)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Resp error: %v\n", err)
		return
	}
	defer resp.Body.Close()

	textRespone, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ReadAll error: %v\n", err)
		return
	}

	fmt.Printf("Main response answer: %s\n", textRespone)

	answerChan := make(chan string, RequestCount)

	// var wg sync.WaitGroup
	tickCount := 5
	for {
		time.Sleep(time.Second)
		if tickCount == 0 {
			break
		}
		fmt.Printf("...%d...\n", tickCount)
		tickCount--
	}

	start := time.Now()

	for i := 0; i < RequestCount; i++ {
		go func(ch chan string, requestNumber int) {
			// defer waitGroup.Done()
			resp, err := http.Get(requestTemplate2)
			if err != nil {
				ch <- fmt.Sprintf("Get error in %d", requestNumber)
				return
			}
			defer resp.Body.Close()
			answer, err := io.ReadAll(resp.Body)
			if err != nil {
				ch <- fmt.Sprintf("ReadAll error in %d", requestNumber)
				return
			}
			ch <- string(answer)
		}(answerChan, i)
		// wg.Add(1)
	}
	// go func(waitGroup *sync.WaitGroup) {
	// 	wg.Wait()
	// 	close(answerChan)
	// }(&wg)

	counter := 0
	for str := range answerChan {
		fmt.Printf("[%d] %s\n", counter, str)
		counter++
		if counter == RequestCount {
			close(answerChan)
		}
	}

	fmt.Printf("Elapsed time: %v\n", time.Since(start))
}
