from aiohttp import web
import aiohttp
from aiohttp.web_request import Request
import fasttext
import pymorphy2
import argparse

programDescription = "[MEME-API] Request analyzer"

def isContainGenLetters(word) -> bool:
  for l in word:
    if l.isupper():
        return True
    else:
        return False

def normalizeRequest(rawRequest, morphAnalyzer):
    requestWords = rawRequest.split("_")
    resultList = []
    for word in requestWords:
        if isContainGenLetters(word):
            resultList.append(word)
        else:
            resultList.append(morphAnalyzer.parse(word)[0].normal_form)
    
    return " ".join(resultList)

async def handle(request: Request):
    rawRequest = request.query.get("request", "")
    if len(rawRequest) > 0:
        normalizedRequest = normalizeRequest(rawRequest, request.app['morph'])
        response = request.app['model'].predict(normalizedRequest)[0][0]
        print(f"{rawRequest} -> {normalizedRequest} -> {response}")
        return web.Response(text=response)
    
    return web.Response(text="empty request")

def main():
    argumentParser = argparse.ArgumentParser(description=programDescription)
    argumentParser.add_argument("-m", "--model", default="./request_model.bin", help="Path to request model")
    argumentParser.add_argument("-d", "--dict",default="./pymorphy2_dicts_ru/data", help="Path to word dictionary [RU]")
    args = argumentParser.parse_args()

    print(programDescription)
    print(f"aiohttp {aiohttp.__version__}")
    print(f"fasttext 'SOME VERSION'")
    print(f"pymorphy2 {pymorphy2.__version__}")

    model = fasttext.load_model(args.model)
    print(f"Model: {args.model}")

    morph = []
    try:
        morph = pymorphy2.MorphAnalyzer(path=args.dict)
    except:
        print(f"ERROR: Can't load {args.dict} dictionary")
        return

    app = web.Application()

    app['morph'] = morph
    app['model'] = model

    app.router.add_get('/', handle)
    web.run_app(app, port=4040)
    print("\nServer stopped by user")

if __name__ == "__main__":
    main()